package org.hillel.java.pro.homeworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

public class FileNavigatorTest {

    FileNavigator fileNavigator = new FileNavigator();
    FileData file1_1 = FileData.builder()
            .size(800).name("file1_1").path("/folder1").build();
    FileData file1_2 = FileData.builder()
            .size(300).name("file1_2").path("/folder1").build();
    FileData file2_1 = FileData.builder()
            .size(700).name("file2_1").path("/folder2").build();

    @BeforeEach
    void init(){
        fileNavigator.add("/folder1", file1_1);

        fileNavigator.add("/folder2", file2_1);

        fileNavigator.add("/folder1", file1_2);
    }
    @Test
    void filterBySizeTest(){

        System.out.println("fileNavigator = " + fileNavigator.toString());

        List<FileData> testList = fileNavigator.filterBySize(800);

        testList.forEach(System.out::println);

        Assertions.assertEquals(800, testList.get(0).getSize());

    }

    @Test
    void removeTest(){

        fileNavigator.remove("/folder2");

        Assertions.assertFalse(fileNavigator.fileMap.containsKey("/folder2"));

    }

    @Test
    void sortBySizeTest(){

        List<FileData> testList = fileNavigator.sortBySize();

        testList.forEach(System.out::println);

        Assertions.assertEquals(300, testList.get(0).getSize());

    }

    @Test
    void addWithExceptionTest(){

        WrongPathFileException ex =
                Assertions.assertThrows(WrongPathFileException.class,
                        () ->fileNavigator.add("/folder1", file2_1));

        Assertions.assertEquals("Incorrect path in file data", ex.getMessage());

        System.out.println(ex.getMessage() + ", expected path : " + ex.getPath()+", path in file data : "+ex.getFilePath());

    }

    @Test
    void findTest(){

        List<FileData> testList = fileNavigator.find("/folder2");

        Assertions.assertNotNull(testList);

        testList = fileNavigator.find("/folder8");

        Assertions.assertNull(testList);

    }
}
