package org.hillel.java.pro.homeworks;

import lombok.Getter;

@Getter
public class WrongPathFileException extends RuntimeException{

    String path;
    String filePath;

      public WrongPathFileException(String message, String path, String filePath) {
            super(message);
            this.filePath = filePath;
            this.path = path;
        }
    }


