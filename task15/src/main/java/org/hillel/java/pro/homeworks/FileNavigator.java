package org.hillel.java.pro.homeworks;

import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ToString
public class FileNavigator {

    public Map<String, List<FileData>> fileMap = new HashMap<String, List<FileData>>();

    public void add(String path, FileData file) throws WrongPathFileException{

        if (!path.equals(file.getPath())) throw new WrongPathFileException("Incorrect path in file data", path, file.getPath());
        if (fileMap.containsKey(path)){
            if (!fileMap.get(path).contains(file)) {
                fileMap.get(path).add(file);
            }
        }
        else {
            List<FileData> list = new ArrayList<>();
            list.add(file);
            fileMap.put(path, list);
        }
    }

    public List<FileData> find(String path){
        return  fileMap.get(path);
    }

    public List<FileData> filterBySize(int size){

        return fileMap.values()
                .stream().flatMap(Collection::stream)
                .filter(c->c.getSize()==size)
                .collect(Collectors.toList());

    }

    public void remove(String path){
        fileMap.remove(path);
    }

    public List<FileData> sortBySize(){

        return fileMap.values()
                .stream().flatMap(Collection::stream)
                .sorted(Comparator.comparing(FileData::getSize))
                .collect(Collectors.toList())
                ;
    }
}
