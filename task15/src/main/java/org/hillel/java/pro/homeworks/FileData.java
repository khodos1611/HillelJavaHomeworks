package org.hillel.java.pro.homeworks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Setter
@Getter
@ToString

public class FileData {
    private String name;
    private int size;
    private String path;

}
