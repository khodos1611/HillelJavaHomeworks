package org.hillel.java.pro.homeworks.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FurnitureFactoryTest {

    @Test
    void furnitureFactoryTest(){

        FurnitureFactory furnitureFactory= new FurnitureFactory();

        Furniture bed = furnitureFactory.build(FurnitureType.BED);
        bed.ready();
        Assertions.assertInstanceOf(Bed.class, bed);
        Assertions.assertEquals("BED", bed.getName());

        Furniture table = furnitureFactory.build(FurnitureType.TABLE);
        table.ready();
        Assertions.assertInstanceOf(Table.class, table);
        Assertions.assertEquals("TABLE", table.getName());

        Furniture chair = furnitureFactory.build(FurnitureType.CHAIR);
        chair.ready();
        Assertions.assertInstanceOf(Chair.class, chair);
        Assertions.assertEquals("CHAIR", chair.getName());

    }
}
