package org.hillel.java.pro.homeworks.startegy;

import org.hillel.java.pro.homeworks.strategy.Figure;
import org.hillel.java.pro.homeworks.strategy.ObtainSquareRectangle;
import org.hillel.java.pro.homeworks.strategy.ObtainSquareTriangle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ObtainSquareTest {

    @Test
    void obtainTriangleSquareTest(){

        Figure triangle = Figure.builder()
                .obtainSquareStrategy(new ObtainSquareTriangle())
                .side1(15)
                .side2(25)
                .name("Triangle")
                .build();

        Assertions.assertEquals(187.5, triangle.getObtainSquareStrategy().getSquare(triangle.getSide1(), triangle.getSide2()));

    }

    @Test
    void obtainRectangleSquareTest(){

        Figure rectangle = Figure.builder()
                .obtainSquareStrategy(new ObtainSquareRectangle())
                .side1(15)
                .side2(25)
                .name("Rectangle")
                .build();

        Assertions.assertEquals(375, rectangle.getObtainSquareStrategy().getSquare(rectangle.getSide1(), rectangle.getSide2()));

    }
}
