package org.hillel.java.pro.homeworks.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarTest {

    @Test
    void carTest(){

        Car buildedCar = Car.builder()
                .detail1("Windscreen")
                .detail2("Boot")
                .detail3("Bumper ")
                .detail4("Bonnet ")
                .wheelCount(4)
                .build();

        Assertions.assertInstanceOf(Car.class, buildedCar);
        Assertions.assertEquals(4, buildedCar.getWheelCount());

    }
}
