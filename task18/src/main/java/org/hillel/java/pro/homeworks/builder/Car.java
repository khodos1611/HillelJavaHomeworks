package org.hillel.java.pro.homeworks.builder;

import lombok.Getter;

@Getter
public class Car {
    private String detail1;
    private String detail2;
    private String detail3;
    private String detail4;
    private int wheelCount;

    public static Builder builder() {
        return new Builder();
    }


   public static class Builder {
        private String detail1;
        private String detail2;
        private String detail3;
        private String detail4;
        private int wheelCount;

        public Builder detail1(String detail) {
            this.detail1 = detail;
            return this;
        }

        public Builder detail2(String detail) {
            this.detail2 = detail;
            return this;
        }

        public Builder detail3(String detail) {
            this.detail3 = detail;
            return this;
        }

        public Builder detail4(String detail) {
            this.detail4 = detail;
            return this;
        }

        public Builder wheelCount(int wheelCount) {
            this.wheelCount = wheelCount;
            return this;
        }

        public Car build() {

            Car car = new Car();
            car.detail1 = this.detail1;
            car.detail2 = this.detail2;
            car.detail3 = this.detail3;
            car.detail4 = this.detail4;
            car.wheelCount = this.wheelCount;
            return car;
        }
    }


}
