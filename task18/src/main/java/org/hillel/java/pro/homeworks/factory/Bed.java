package org.hillel.java.pro.homeworks.factory;

public class Bed extends Furniture{
    @Override
    public void ready() {
        System.out.println("Bed is ready");
    }

    public Bed() {
        super.setName("BED");
    }
}
