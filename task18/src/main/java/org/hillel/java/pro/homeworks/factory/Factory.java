package org.hillel.java.pro.homeworks.factory;

public interface Factory<T>{

 T build(FurnitureType type);
}
