package org.hillel.java.pro.homeworks.factory;

public enum FurnitureType {

    TABLE,
    CHAIR,
    BED

}
