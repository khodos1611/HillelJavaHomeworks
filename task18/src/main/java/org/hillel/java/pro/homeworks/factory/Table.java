package org.hillel.java.pro.homeworks.factory;

public class Table extends Furniture{

    @Override
    public void ready() {
        System.out.println("Table is ready");
    }

    public Table() {
        super.setName("TABLE");
    }
}
