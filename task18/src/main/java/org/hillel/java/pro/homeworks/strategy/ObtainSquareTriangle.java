package org.hillel.java.pro.homeworks.strategy;

public class ObtainSquareTriangle implements ObtainSquare{
    @Override
    public double getSquare(int side1, int side2) {
        return (side1*side2)/2.0;
    }
}
