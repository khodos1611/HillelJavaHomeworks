package org.hillel.java.pro.homeworks.factory;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Furniture {

    private String name;
    public void ready() {
        System.out.println(" Furniture is ready ");
    }
}
