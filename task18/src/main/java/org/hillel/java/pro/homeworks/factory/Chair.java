package org.hillel.java.pro.homeworks.factory;

public class Chair extends Furniture{
    @Override
    public void ready() {
        System.out.println("Chair is ready");
    }

    public Chair() {
        super.setName("CHAIR");
    }
}
