package org.hillel.java.pro.homeworks.strategy;

public interface ObtainSquare {

    double getSquare(int side1, int side2);
}
