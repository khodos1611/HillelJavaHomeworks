package org.hillel.java.pro.homeworks.factory;

public class FurnitureFactory implements Factory<Furniture>{
    @Override
    public Furniture build(FurnitureType type) {
        return switch (type) {
            case BED -> new Bed();
            case CHAIR -> new Chair();
            case TABLE -> new Table();
        };

    }
}
