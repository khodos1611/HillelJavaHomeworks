package org.hillel.java.pro.homeworks.strategy;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Figure {

    private ObtainSquare obtainSquareStrategy;
    private int side1;
    private int side2;
    private String name;

}
