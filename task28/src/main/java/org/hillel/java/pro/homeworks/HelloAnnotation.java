package org.hillel.java.pro.homeworks;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns ="/hello_ann")
public class HelloAnnotation extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>Hello annotation servlet</title>");
        writer.println("</head>");

        writer.println("<h1>ANNOTATION</h1>");
        writer.println("<h1>My first annotation servlet "+name+"</h1>");
        writer.println("</body>");
        writer.println("</html>");
    }
}
