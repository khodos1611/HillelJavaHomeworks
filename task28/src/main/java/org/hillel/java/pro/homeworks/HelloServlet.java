package org.hillel.java.pro.homeworks;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {

    public HelloServlet() {}

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>Hello world</title>");
        writer.println("</head>");
        writer.println("<body bgcolor=white>");

        writer.println("<h1>Hello world</h1>");
        writer.println("<h1>My first servlet</h1>");
        writer.println("</body>");
        writer.println("</html>");
    }
}
