package org.hillel.java.pro.homeworks.enums;

import com.mysql.cj.jdbc.MysqlDataSource;
import lombok.SneakyThrows;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;


public enum DataBaseConnection {

    INSTANCE;
    private final DataSource dataSource;

    @SneakyThrows
    DataBaseConnection() {
        this.dataSource = getDataSource();
    }

    @SneakyThrows
    private DataSource getDataSource(){
        PropertiesReader propertiesReader = new PropertiesReader("application.properties");

        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setURL(propertiesReader.getProperty("url"));
        mysqlDataSource.setUser(propertiesReader.getProperty("user"));
        mysqlDataSource.setPassword(propertiesReader.getProperty("pass"));

        return mysqlDataSource;
    }
    @SneakyThrows
    public Connection getConnection(){
        return dataSource.getConnection();
    }

    @SneakyThrows
    public void closeConnection(Connection connection){
        connection.close();
    }

    public static class PropertiesReader {
        private final Properties properties;

        @SneakyThrows
        public PropertiesReader(String propertyFileName) {
            InputStream is = getClass().getClassLoader()
                    .getResourceAsStream(propertyFileName);
            this.properties = new Properties();
            this.properties.load(is);
        }

        public String getProperty(String propertyName) {
            return this.properties.getProperty(propertyName);
        }
    }
}
