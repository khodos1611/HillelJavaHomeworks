package org.hillel.java.pro.homeworks.repository;

import lombok.SneakyThrows;
import org.hillel.java.pro.homeworks.entity.Homework;
import org.hillel.java.pro.homeworks.enums.DataBaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HomeworkRepository implements EducationRepository<Homework> {


    @SneakyThrows
    @Override
    public List<Homework> getAll() {

        List<Homework> list = new ArrayList<>();

        String query = "SELECT * FROM HOMEWORK";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                Homework homework = Homework.builder()
                        .id((long) resultSet.getInt("id"))
                        .description(resultSet.getString("description"))
                        .name(resultSet.getString("name"))
                        .build();

                list.add(homework);
            }
        }

        return list;

    }

    @SneakyThrows
    @Override
    public Optional get(Long id) {

        String query = "SELECT * FROM HOMEWORK WHERE id=(?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)){

            statement.setLong(1,id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Homework homework = Homework.builder()
                        .id((long) resultSet.getInt("id"))
                        .description(resultSet.getString("description"))
                        .name(resultSet.getString("name"))
                        .build();

                return  Optional.of(homework);
            }
        }
        return Optional.empty();
    }

    @Override
    public void update(Homework item) {

    }

    @SneakyThrows
    @Override
    public void add(Homework item) {
        String insert = "INSERT INTO homework(name, description) VALUES (?,?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(insert)) {

            statement.setString(1, item.getName());
            statement.setString(2, item.getDescription());
            statement.execute();

        }
    }
    @SneakyThrows
    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM homework WHERE id =(?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(delete)){

            statement.setLong(1,id);
            statement.executeUpdate();

        }
    }

    public Homework getHomeworkByID(Long id) {

        Optional optional = this.get(id);
        if (optional.isPresent()) return (Homework) optional.get();
        else return new Homework();
    }
}
