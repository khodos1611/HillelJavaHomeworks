package org.hillel.java.pro.homeworks.repository;

import java.util.List;
import java.util.Optional;

public interface EducationRepository<T>{

    List<T> getAll();
    Optional<T> get(Long id);

    void update(T item);

    void add(T item);

    void delete(Long id);
}
