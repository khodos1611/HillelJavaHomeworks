package org.hillel.java.pro.homeworks.repository;

import lombok.SneakyThrows;
import org.hillel.java.pro.homeworks.entity.Lesson;
import org.hillel.java.pro.homeworks.enums.DataBaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonRepository implements EducationRepository<Lesson> {
    @SneakyThrows
    @Override
    public List<Lesson> getAll() {

        HomeworkRepository homeworkRepository = new HomeworkRepository();
        List<Lesson> list = new ArrayList<>();

        String query = "SELECT * FROM LESSON";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {

                Lesson lesson = Lesson.builder()
                        .id((long) resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .homework(homeworkRepository.getHomeworkByID((long) resultSet.getInt("homework_id")))
                        .build();

                list.add(lesson);
            }
        }

        return list;

    }

    @SneakyThrows
    @Override
    public Optional get(Long id) {
        HomeworkRepository homeworkRepository = new HomeworkRepository();
        String query = "SELECT * FROM LESSON WHERE id=(?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Lesson lesson = Lesson.builder()
                        .id((long) resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .homework(homeworkRepository.getHomeworkByID((long) resultSet.getInt("homework_id")))
                        .build();

                return Optional.of(lesson);
            }
        }
        return Optional.empty();
    }

    @Override
    public void update(Lesson item) {

    }

    @SneakyThrows
    @Override
    public void add(Lesson item) {
        String insert = "INSERT INTO lesson(name, homework_id) VALUES (?,?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(insert)) {

            statement.setString(1, item.getName());
            statement.setLong(2, item.getHomework().getId());

            statement.execute();

        }
    }

    @SneakyThrows
    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM LESSON WHERE id =(?)";
        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(delete)) {

            statement.setLong(1, id);
            statement.executeUpdate();

        }
    }


}
