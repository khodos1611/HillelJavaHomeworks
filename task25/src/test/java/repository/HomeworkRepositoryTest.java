package repository;

import org.hillel.java.pro.homeworks.repository.HomeworkRepository;
import org.hillel.java.pro.homeworks.entity.Homework;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class HomeworkRepositoryTest {

    @Test
    void testAdd(){

        Homework homework = Homework.builder()
                .name("Homework_Task25")
                .description("Homework_Task25").
                build();

        HomeworkRepository homeworkRepository = new HomeworkRepository();
        List<Homework> listBefore = homeworkRepository.getAll();
        homeworkRepository.add(homework);
        List<Homework> listAfter = homeworkRepository.getAll();
        Assertions.assertEquals(listBefore.size()+1, listAfter.size());

    }
    @Test
    void testGetAll(){
        HomeworkRepository homeworkRepository = new HomeworkRepository();
        List<Homework> list = homeworkRepository.getAll();

        list.forEach(l-> System.out.println(l.toString()));

        Assertions.assertTrue(list.size()>0);
    }
    @Test
    void testGet(){

        HomeworkRepository homeworkRepository = new HomeworkRepository();
        Optional homework = homeworkRepository.get(2L);

        Assertions.assertInstanceOf(Homework.class, homework.get());
        Assertions.assertFalse(homework.isEmpty());

    }
    @Test
    void testDelete(){
        HomeworkRepository homeworkRepository = new HomeworkRepository();
        List<Homework> listBefore = homeworkRepository.getAll();
        homeworkRepository.delete(10L);
        List<Homework> listAfter = homeworkRepository.getAll();
        Assertions.assertEquals(listBefore.size()-1, listAfter.size());
    }
}
