package repository;
import org.hillel.java.pro.homeworks.entity.Lesson;
import org.hillel.java.pro.homeworks.repository.HomeworkRepository;
import org.hillel.java.pro.homeworks.repository.LessonRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class LessonRepositoryTest {

    @Test
    void testAdd(){

        Lesson lesson = Lesson.builder()
                .name("lesson2")
                .homework(new HomeworkRepository().getHomeworkByID(5L)).
                build();

        LessonRepository lessonRepository = new LessonRepository();
        List<Lesson> listBefore = lessonRepository.getAll();
        lessonRepository.add(lesson);
        List<Lesson> listAfter = lessonRepository.getAll();
        Assertions.assertEquals(listBefore.size()+1, listAfter.size());

    }
    @Test
    void testGetAll(){
        LessonRepository lessonRepository = new LessonRepository();
        List<Lesson> list = lessonRepository.getAll();

        list.forEach(l-> System.out.println(l.toString()));

        Assertions.assertTrue(list.size()>0);
    }
    @Test
    void testGet(){

        LessonRepository lessonRepository = new LessonRepository();
        Optional lesson = lessonRepository.get(2L);

        Assertions.assertInstanceOf(Lesson.class, lesson.get());
        Assertions.assertFalse(lesson.isEmpty());

    }
    @Test
    void testDelete(){
        LessonRepository lessonRepository = new LessonRepository();
        List<Lesson> listBefore = lessonRepository.getAll();
        lessonRepository.delete(10L);
        List<Lesson> listAfter = lessonRepository.getAll();
        Assertions.assertEquals(listBefore.size()-1, listAfter.size());
    }
}
