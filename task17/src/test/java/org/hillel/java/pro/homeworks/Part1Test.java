package org.hillel.java.pro.homeworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class Part1Test {

    @Test
    void getListTest(){

        Integer[] array = {5,3,6,7,7};
        List<?> list= Part1.getList(array);
        list.forEach(System.out::println);
        Assertions.assertEquals(list.get(0).getClass(), Integer.class);

        String[] arrayString = {"one","3","6","7","7"};
        list = Part1.getList(arrayString);
        list.forEach(System.out::println);
        Assertions.assertEquals(list.get(0).getClass(), String.class);

    }
}
