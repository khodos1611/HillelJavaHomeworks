package org.hillel.java.pro.homeworks.FruitBox;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class BoxTest {
    Box<Apple> appleBox = new Box<>();
    Box<Apple> appleBox2 = new Box<>();
    Box<Orange> orangeBox = new Box<>();

    @Test
    @BeforeEach
    void addFruitTest(){

        appleBox.getFruit().clear();
        appleBox2.getFruit().clear();
        orangeBox.getFruit().clear();

        appleBox.addFruit(new Apple());
        appleBox.addFruit(new Apple());
        appleBox.addFruit(new Apple[]{new Apple(), new Apple()});

        appleBox2.addFruit(new Apple());
        appleBox2.addFruit(new Apple());
        appleBox2.addFruit(new Apple());
        appleBox2.addFruit(new Apple());
        appleBox2.addFruit(new Apple[]{new Apple(), new Apple()});

        orangeBox.addFruit(new Orange());
        orangeBox.addFruit(new Orange[]{new Orange(), new Orange(), new Orange()});

        Assertions.assertEquals(4, appleBox.getFruit().size());
        Assertions.assertEquals(6, appleBox2.getFruit().size());

    }

    @Test
    void getWeightTest(){
        Assertions.assertEquals(4, appleBox.getWeight());
        Assertions.assertEquals(6, appleBox2.getWeight());
    }

    @Test
    void compareTest(){
        Assertions.assertFalse(orangeBox.compare(appleBox));
        Assertions.assertTrue(orangeBox.compare(appleBox2));
    }
    @Test
    void mergeTest(){
        appleBox.merge(appleBox2);
        Assertions.assertEquals(10, appleBox.getFruit().size());
    }
}
