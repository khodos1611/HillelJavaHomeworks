package org.hillel.java.pro.homeworks.FruitBox;

public class Orange extends Fruit{

    public static final double WEIGHT = 1.5F;

    @Override
    public double getWeight() {
        return WEIGHT;
    }
}
