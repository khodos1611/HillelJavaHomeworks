package org.hillel.java.pro.homeworks.FruitBox;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class Box<T extends Fruit> {

    private List<T> fruit;

    public Box() {
        this.fruit = new ArrayList<>();
    }

    public void addFruit(T item){

        fruit.add(item);

    }

    public void addFruit(T[] items){

        Collections.addAll(fruit, items);

    }

    public double getWeight(){

        if(fruit.size()==0) return 0;

        return fruit.size()* fruit.get(0).getWeight();
    }

    public boolean compare(Box<?> box){
        return getWeight()==box.getWeight();
    }

    public void merge(Box<T> source){

        fruit.addAll(source.fruit);

    }
}
