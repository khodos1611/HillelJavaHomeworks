package org.hillel.java.pro.homeworks.FruitBox;


public class Apple extends Fruit {

    public static final double WEIGHT = 1.0F;

    @Override
    public double getWeight() {
        return WEIGHT;
    }
}
