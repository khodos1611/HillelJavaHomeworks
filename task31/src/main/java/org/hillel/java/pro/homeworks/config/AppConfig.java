package org.hillel.java.pro.homeworks.config;

import org.hillel.java.pro.homeworks.service.Cart;
import org.hillel.java.pro.homeworks.entity.Product;
import org.hillel.java.pro.homeworks.service.ProductRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@ComponentScan(basePackages = "org.hillel.java.pro.homeworks")
public class AppConfig {
    @Bean
    public ProductRepository getProductRepository(){

        ProductRepository productRepository=  new ProductRepository();

        productRepository.setProductList(Stream.of(
                new Product(1L,   "milk", 2.50),
                new Product(2L,   "meat", 12.50),
                new Product(3L,  "bread", 3.50),
                new Product(4L,  "lemon", 1.50),
                new Product(5L,  "apple", 0.50),
                new Product(6L, "butter", 5.00))
                .collect(Collectors.toList()));

        return  productRepository;
    }

    @Bean
    @Scope("prototype")
    public Cart getCart(){
        return  new Cart();
    }


}