package org.hillel.java.pro.homeworks.service;

import lombok.Getter;
import lombok.Setter;
import org.hillel.java.pro.homeworks.entity.Product;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ProductRepository {

    private List<Product> productList = new ArrayList<>();

    public Product getByID(Long id){

        return productList.stream().filter(c->c.getId().equals(id)).findFirst().orElse(null);

    }



}
