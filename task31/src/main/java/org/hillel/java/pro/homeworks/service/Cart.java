package org.hillel.java.pro.homeworks.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hillel.java.pro.homeworks.entity.Product;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Cart {

    private Map<Long, Product> customerCart = new HashMap<>();
    private ProductRepository repository;

    public void addToCard(Long id){
        Product product = repository.getByID(id);
        if(product==null) {
            System.out.println("Product with ID "+id+" not contains in product list");
            return;
        }

        if (!customerCart.containsKey(id)) {
            customerCart.put(id, product);
            System.out.println("Product "+product.getName()+" successfully added to cart.");
        }
        else System.out.println("Product "+product.getName()+" with ID " + id + " already added to cart.");
    }

    public void removeFromCard(Long id){

        Product product = repository.getByID(id);
        if(product==null) {
            System.out.println("Product with ID "+id+" not contains in product list");
            return;
        }

        if (customerCart.containsKey(id)) {
            customerCart.remove(id);
            System.out.println("Product "+product.getName()+" removed from the cart.");
        }
        else System.out.println("Cart not contain product with ID " + id+".");
    }

    public double getTotalCost(){

        return customerCart.values().stream().mapToDouble(Product::getCost).sum();
    }

}
