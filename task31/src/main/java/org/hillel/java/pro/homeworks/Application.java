package org.hillel.java.pro.homeworks;

import org.apache.commons.lang3.StringUtils;
import org.hillel.java.pro.homeworks.config.AppConfig;
import org.hillel.java.pro.homeworks.service.Cart;
import org.hillel.java.pro.homeworks.entity.Product;
import org.hillel.java.pro.homeworks.service.ProductRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {


        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        ProductRepository productRepository = context.getBean(ProductRepository.class);

        List<Product> productList = productRepository.getProductList();

        System.out.println("Our products :");
        productList.forEach(System.out::println);
        System.out.println("To add to cart product type \"+\" and product ID");
        System.out.println("To remove product from cart type \"-\" and product ID");
        System.out.println("To exit type E");


        Cart cart = context.getBean(Cart.class);
        cart.setRepository(productRepository);

        Scanner scanner= new Scanner(System.in);
        String userInput = scanner.next();

        while (!userInput.equals("E")){
            userInput = handleUserInput(scanner,userInput, cart);
        }
        System.out.println("Total cost is "+cart.getTotalCost());
        System.out.println("Bye!");
    }

    public static String handleUserInput(Scanner scanner, String userInput, Cart cart){
        try {
            if(StringUtils.left(userInput,1).equals("+"))
                cart.addToCard(Long.parseLong(StringUtils.substring(userInput,1)));
            else if (StringUtils.left(userInput,1).equals("-"))
                cart.removeFromCard(Long.parseLong(StringUtils.substring(userInput,1)));
            else
                System.out.println("Wrong input, try again");
        }
        catch (NumberFormatException ex) {
            System.out.println("Wrong input, try again");
        }

        return  scanner.next();
    }
}