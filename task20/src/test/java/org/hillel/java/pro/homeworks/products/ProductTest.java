package org.hillel.java.pro.homeworks.products;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.List;
import static java.time.Month.APRIL;

public class ProductTest {

    ProductsService productsService = new ProductsService();

    @BeforeEach
    void init(){

        productsService.productList.clear();
        productsService.productList.add(Product.builder()
                .isSale(true)
                .type("book")
                .id("1")
                .price(500)
                .date(LocalDate.of(2023,3,10))
                .build());
        productsService.productList.add(Product.builder()
                .isSale(true)
                .type("book")
                .id("2")
                .price(70)
                .date(LocalDate.of(2023,4,15))
                .build());
        productsService.productList.add(Product.builder()
                .isSale(false)
                .type("card")
                .id("3")
                .price(1)
                .date(LocalDate.of(2023,1,11))
                .build());
        productsService.productList.add(Product.builder()
                .isSale(false)
                .type("journal")
                .id("4")
                .price(5)
                .date(LocalDate.of(2022,3,10))
                .build());
        productsService.productList.add(Product.builder()
                .isSale(false)
                .type("book")
                .id("1")
                .price(60)
                .date(LocalDate.of(2022,2,9))
                .build());


    }
    @Test
    void filterByCategoryAndPriceTest() {

        Assertions.assertEquals(1, productsService.filterByCategoryAndPrice("book", 250).size());

    }

    @Test
    void getProductListWithDiscountTest(){

        List<Product> productList = productsService.getProductListWithDiscount("book", 10);
        Assertions.assertEquals(2, productList.size());
        Assertions.assertEquals(450.0, productList.get(0).getPrice());

    }

    @Test
    void getCheapestProductTest(){

       Assertions.assertEquals(60, productsService.getCheapestProduct("book").getPrice());
       Assertions.assertThrows(ProductNotFoundException.class, ()-> productsService.getCheapestProduct( "paper"));

    }

    @Test
    void getThreeLastAddedProductsTest(){

        Assertions.assertEquals(3, productsService.getThreeLastAddedProducts().size());
        Assertions.assertEquals(APRIL, productsService.getThreeLastAddedProducts().get(0).getDate().getMonth());

    }

    @Test
    void getTotalCostTest(){

        Assertions.assertEquals(70, productsService.getTotalCost("book", 75));
    }

    @Test
    void getGroupingByTypeProductsTest(){

        Assertions.assertEquals(3, productsService.getGroupingByTypeProducts().keySet().size());
    }
}
