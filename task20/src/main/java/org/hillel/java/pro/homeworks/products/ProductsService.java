package org.hillel.java.pro.homeworks.products;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;

@Getter
@Setter
public class ProductsService {

    List<Product> productList = new ArrayList<>();

    public void add(Product product){
        productList.add(product);
    }

    public void sell(Product product){
        productList.remove(product);
    }

    public List<Product> filterByCategoryAndPrice(String type, double price){

        return productList.stream()
                .filter(c -> c.getType().equals(type) && c.getPrice() > price)
                .collect(Collectors.toList());

    }

    public List<Product> getProductListWithDiscount(String type, int discountPercent){

        return  productList.stream()
                .filter(c -> c.getType().equals(type) && c.isSale())
                .peek(c -> c.setPrice(c.getPrice() *(100-discountPercent)/100))
                .collect(Collectors.toList());

    }
    public Product getCheapestProduct( String type) throws ProductNotFoundException{

        return  productList.stream()
                .filter(c -> c.getType().equals(type))
                .min(Comparator.comparing(Product::getPrice))
                .orElseThrow(()->new ProductNotFoundException("Product ["+type+"] not found"));

    }

    public List<Product> getThreeLastAddedProducts(){

        return productList.stream()
                        .sorted(Collections.reverseOrder(Comparator.comparing(Product::getDate)))
                        .limit(3)
                        .collect(Collectors.toList());

    }

    public double getTotalCost(String type, double price){

        return productList.stream()
                .filter(c -> c.getType().equals(type)
                        &&c.getDate().getYear() == LocalDate.now().getYear()
                        &&c.getPrice()<=price)
                .mapToDouble(Product::getPrice)
                .sum();


    }
    public Map<String, List<Product>> getGroupingByTypeProducts(){

        return productList.stream()
                .collect(groupingBy(Product::getType));

    }

}
