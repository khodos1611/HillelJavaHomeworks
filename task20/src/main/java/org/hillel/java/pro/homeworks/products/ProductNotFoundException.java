package org.hillel.java.pro.homeworks.products;

import lombok.Getter;

@Getter
public class ProductNotFoundException extends RuntimeException{

    public ProductNotFoundException(String message) {
        super(message);
    }
}


