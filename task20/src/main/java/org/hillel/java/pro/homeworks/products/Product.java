package org.hillel.java.pro.homeworks.products;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@Builder
public class Product {

    private String type;

    private double price;

    private boolean isSale;

    private LocalDate date;

    private String id;
}
