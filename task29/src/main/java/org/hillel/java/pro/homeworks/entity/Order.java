package org.hillel.java.pro.homeworks.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@ToString
@AllArgsConstructor


public class Order {

    private UUID id;

    private LocalDate date;
    private double cost;

    private List<Product> products;

    public Order() {
        this.id = UUID.randomUUID();
        this.cost = 0;
        this.date = LocalDate.now();
        this.products = new ArrayList<>();
    }
}
