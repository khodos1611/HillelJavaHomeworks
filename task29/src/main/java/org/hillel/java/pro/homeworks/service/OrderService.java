package org.hillel.java.pro.homeworks.service;

import org.hillel.java.pro.homeworks.entity.Order;
import org.hillel.java.pro.homeworks.repository.OrderRepository;

import java.util.Collection;
import java.util.UUID;

public class OrderService {

    OrderRepository repository = new OrderRepository();

    public void add(Order order) {
        repository.add(order);
    }

    public Order getById(UUID id) {

        return repository.getById(id);
    }

    public Collection<Order> getAll() {

        return repository.getAll();
    }

}
