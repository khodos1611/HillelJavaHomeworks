package org.hillel.java.pro.homeworks.controller;


import org.hillel.java.pro.homeworks.entity.Order;
import org.hillel.java.pro.homeworks.service.OrderService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/orders")
public class OrderController {

    OrderService orderService = new OrderService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrder(Order order){

        orderService.add(order);

        return Response.ok(order).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrder(@PathParam("id") UUID uuid){

        Order order = orderService.getById(uuid);

        return Response.ok(order).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(){

        return Response.ok(orderService.getAll()).build();
    }
}
