package org.hillel.java.pro.homeworks.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class Product {

    private UUID id;
    private String name;
    private double cost;

    public Product(String name, double cost) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.cost = cost;
    }
}
