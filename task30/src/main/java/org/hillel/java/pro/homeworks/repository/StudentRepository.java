package org.hillel.java.pro.homeworks.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.hillel.java.pro.homeworks.entity.Student;
import org.hillel.java.pro.homeworks.enums.HibernateSession;

import java.util.List;


public class StudentRepository {


    public Student getByID(Long id){
        try(Session session = HibernateSession.INSTANCE.openSession()){
            return session.get(Student.class, id);
        }
    }

    public List<Student> getAll(){
        try(Session session = HibernateSession.INSTANCE.openSession()){

            HibernateCriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            JpaCriteriaQuery<Student> query = criteriaBuilder.createQuery(Student.class);

            query.from(Student.class);

            return session.createQuery(query).getResultList();
        }
    }

    public void add(Student student){
        try(Session session = HibernateSession.INSTANCE.openSession()){
            Transaction transaction = session.beginTransaction();
            session.persist(student);
            transaction.commit();
            }
    }

    public void update(Student student){
        try(Session session = HibernateSession.INSTANCE.openSession()){
            Transaction transaction = session.beginTransaction();
            session.update(student);
            transaction.commit();
        }
    }

    public void delete(Long id){
        try(Session session = HibernateSession.INSTANCE.openSession()){
            Transaction transaction = session.beginTransaction();
            session.delete(session.get(Student.class, id));
            transaction.commit();
        }
    }


}
