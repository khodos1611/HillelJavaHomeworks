package org.hillel.java.pro.homeworks.enums;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hillel.java.pro.homeworks.entity.Student;


public enum HibernateSession {

    INSTANCE;

    private final SessionFactory sessionFactory;

    HibernateSession(){

       sessionFactory = new Configuration().configure().addAnnotatedClass(Student.class).buildSessionFactory();

    }

    public Session openSession(){

        return sessionFactory.openSession();
    }
}
