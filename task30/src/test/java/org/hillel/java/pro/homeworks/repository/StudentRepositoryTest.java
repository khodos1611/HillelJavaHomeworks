package org.hillel.java.pro.homeworks.repository;

import com.github.javafaker.Faker;
import org.hillel.java.pro.homeworks.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;


public class StudentRepositoryTest {

    StudentRepository repository = new StudentRepository();
    Faker faker = new Faker();
    @Test
    void getByIDTest(){

        Student student=repository.getByID(2L);
        Assertions.assertEquals("tbzp56@gmail.com", student.getEmail());

    }
    @Test
    void getAllTest(){

        List<Student> list = repository.getAll();
        System.out.println(list);
        Assertions.assertEquals(6, list.size());
    }

    @Test
    void addTest(){



        for (int i = 0; i < 100; i++) {

            Student student = new Student().builder()
                    .name(faker.artist().name())
                    .email(faker.bothify("????##@gmail.com"))
                    .build();
            repository.add(student);
        }
    }

    @Test
    void updateTest(){

        Student student = repository.getByID(2L);
        student.setName(faker.harryPotter().character());
        repository.update(student);

    }

    @Test
    void deleteTest(){
       repository.delete(3L);
    }

}
