package org.hillel.pro.homeworks.task7;

import java.util.Arrays;
import java.util.Scanner;

public class HomeWorkApp {

    public static void main(String[] args) {

        System.out.println("Symbol 's' occur in word 'Subsequent' " + findSymbolOccurance("Subsequent", 's'));
        System.out.println("Symbol 'S' occur in word 'Subsequent' " + findSymbolOccurance("Subsequent", 'S'));
        System.out.println("Symbol 'Q' occur in word 'Subsequent' " + findSymbolOccurance("Subsequent", 'Q'));
        System.out.println("===========");

        System.out.println("The position of 'ollo' in word 'Apollo' is " + findWordPosition("Apollo","ollo"));
        System.out.println("===========");

        System.out.println("The reverse version of 'Apollo' is :" + stringReverse("Apollo"));
        System.out.println("===========");

        System.out.println("Is the 'ollo' a palindrome ? : "+ isPalindrome("ollo"));
        System.out.println("Is the 'banana' a palindrome ? : " +isPalindrome("banana"));
        System.out.println("===========");

        guessWordGame();


    }

    public static void guessWordGame(){

        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado" , "broccoli", "carrot",
                "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

        System.out.println("Words to guess : " + Arrays.toString(words));

        String guessWord = words[getIndexOfGuessWord(words.length)];

        //System.out.println("guessWord = " + guessWord);

        String userWord = "";

        while (!guessWord.equals(userWord)){
            userWord = inputUserWord(guessWord, userWord);
        }

        System.out.println("Congratulations! You guessed!");

    }
    public static String inputUserWord(String source, String target){

        Scanner scanner= new Scanner(System.in);

        if (target.equals("")) {
            System.out.println("Input your word :");
            target = scanner.next();
        }

        StringBuilder stringToOutput = new StringBuilder();

        int minLengthOfStrings = Math.min(source.length(), target.length());

        for (int i = 0; i < minLengthOfStrings; i++) {
            if (source.charAt(i) == target.charAt(i)) stringToOutput.append(source.charAt(i));
            else stringToOutput.append("#");
        }
        for (int i = minLengthOfStrings; i < 15; i++) stringToOutput.append("#");

        System.out.println("Your result :" );
        System.out.println(stringToOutput);
        System.out.println("Next turn) input your word :" );
        return scanner.next();

    }
    public static int getIndexOfGuessWord(int length){
        return (int) (Math.random()*(length+1));
    }
    public static int findSymbolOccurance(String string, char ch){

        char[] arrayOfChar = string.toLowerCase().toCharArray();

        int countOfOccurance =0;

        for (char s: arrayOfChar) if (s == Character.toLowerCase(ch)) countOfOccurance++;

        return countOfOccurance;
    }

    public static int findWordPosition(String source, String target){

        return source.toLowerCase().indexOf(target.toLowerCase());
    }

    public static String stringReverse(String string){

        StringBuffer stringBuffer = new StringBuffer(string);

        return stringBuffer.reverse().toString();
    }

    public static boolean isPalindrome(String string){

        return string.equals(stringReverse(string));
    }



}

