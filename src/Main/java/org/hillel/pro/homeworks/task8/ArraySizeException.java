package org.hillel.pro.homeworks.task8;

public class ArraySizeException extends RuntimeException {
    public ArraySizeException(String message) {
        super(message);
    }
}