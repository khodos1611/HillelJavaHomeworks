package org.hillel.pro.homeworks.task8;

public class ArrayValueCalculator {

    public  int doCalc(String[][] array) throws ArraySizeException,ArrayDataException{

        if (array.length != 4 )
            throw new ArraySizeException("Wrong size array in parameters. Array must be 4 * 4");

        for (String[] subArray: array) {
            if (subArray.length != 4 )
                throw new ArraySizeException("Wrong size array in parameters. Array must be 4 * 4");
        }
        int result =0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                try {
                    result +=  Integer.parseInt(array[i][j]);
                }
                catch (NumberFormatException ex){
                    throw new ArrayDataException("Invalid data",ex,i,j);
                }
            }
        }

        return result;



    }
}
