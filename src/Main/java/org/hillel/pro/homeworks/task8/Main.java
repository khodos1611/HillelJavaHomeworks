package org.hillel.pro.homeworks.task8;

public class Main {

    public static void main(String[] args) {

        String[][][] arrays = new String[4][][];

        fillArray(arrays);

        ArrayValueCalculator arrayValueCalculator = new ArrayValueCalculator();

        try {
            for (String[][] array: arrays) {

                System.out.println("The sum of the contents of the array  = "
                        + arrayValueCalculator.doCalc(array));

            }
        }
        catch (ArraySizeException ex)
        {
            System.out.println("Message: " + ex.getMessage());
        }
        catch (ArrayDataException ex)
        {
            System.out.println("Message: " + ex.getMessage());
            System.out.println("indexes: row :" + ex.getRowIndex()+", column : "+ex.getColumnIndex());
        }

    }

    public static void fillArray(String[][][] arrayToFill){
        arrayToFill[0] = new String[][] {{"8","6","9","4"},{"8","4","3","7","3"},{"8","6","3","7"}};
        arrayToFill[1] = new String[][] {{"8","6","9","4"},{"8","6","9","4"},{"8","6","9","4"},{"8","4","3","7","3"},{"8","6","3","7"}};
        arrayToFill[2] = new String[][] {{"8","6","9","4"},{"8","6","m","4"},{"8","4","7","3"},{"8","6","3","7"}};
        arrayToFill[3] = new String[][] {{"8","6","9","4"},{"8","6","1","4"},{"8","4","7","3"},{"8","6","3","7"}};
    }
}
