package org.hillel.pro.homeworks.task8;

public class ArrayDataException extends RuntimeException{
    int rowIndex, columnIndex;


    public int getRowIndex() {
        return rowIndex;
    }
    public int getColumnIndex() {
        return columnIndex;
    }

    public ArrayDataException(String message, Throwable cause, int rowIndex, int columnIndex) {
        super(message, cause);
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }
}
