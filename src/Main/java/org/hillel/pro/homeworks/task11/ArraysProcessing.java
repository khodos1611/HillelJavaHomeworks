package org.hillel.pro.homeworks.task11;

import org.apache.commons.lang3.ArrayUtils;

public class ArraysProcessing {

    public int[] getNewArray(int[] source) throws RuntimeException{

        if (!ArrayUtils.contains(source, 4)){
            throw new RuntimeException("Array does not contain 4");
        }

        int index = getStartIndex(source)+1;

        if (index==source.length){
            throw new RuntimeException("4 - is a last element of array, no elements to add to result");
        }
        int[] resultArray = new int[source.length-index];

        for (int i = index; i <=source.length-1; i++) {
            resultArray[i-index] = source[i];
        }

        return resultArray;
    }

    private int getStartIndex(int[] array){

        int index=0;

        for (int i = array.length-1; i >=0; i--) {
            if(array[i] == 4) index=i;
        }
        return index;
    }

    public boolean checkArray(int[] source){

        if (!ArrayUtils.contains(source, 1)) return false;
        if (!ArrayUtils.contains(source, 4)) return false;

        for (int i = 0; i < source.length; i++) {

            if ((source[i]!=4)&&(source[i]!=1)) return false;
        }
        return true;
    }


}
