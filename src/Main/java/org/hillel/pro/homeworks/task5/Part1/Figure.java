package org.hillel.pro.homeworks.task5.Part1;

public interface Figure {

    double getArea();
}
