package org.hillel.pro.homeworks.task11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

public class ArraysProcessingIT {

    @ParameterizedTest
    @MethodSource("getTestArrays")
    void testGetNewArray(int[] testArraysForGet){

        ArraysProcessing arraysProcessing = new ArraysProcessing();

        try {
            System.out.println("source array = " + Arrays.toString(testArraysForGet));
            System.out.println("new array is : " + Arrays.toString(arraysProcessing.getNewArray(testArraysForGet)));
        }
        catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }

        RuntimeException ex =
                Assertions.assertThrows(RuntimeException.class, () -> arraysProcessing.getNewArray(testArraysForGet));
        Assertions.assertEquals("Array does not contain 4", ex.getMessage());

        Assertions.assertArrayEquals(new int[]{5,6,7,2} ,arraysProcessing.getNewArray(testArraysForGet));

        RuntimeException ex2 =
                Assertions.assertThrows(RuntimeException.class, () -> arraysProcessing.getNewArray(testArraysForGet));
        Assertions.assertEquals("4 - is a last element of array, no elements to add to result", ex2.getMessage());


    }

    @ParameterizedTest
    @MethodSource("getTestArrays")
    void testCheckArray(int[] testArraysForCheck){

        ArraysProcessing arraysProcessing = new ArraysProcessing();

        System.out.println("source array = " + Arrays.toString(testArraysForCheck));
        System.out.println("check result: " + arraysProcessing.checkArray(testArraysForCheck));


        Assertions.assertFalse(arraysProcessing.checkArray(testArraysForCheck));
        Assertions.assertTrue(arraysProcessing.checkArray(testArraysForCheck));
        Assertions.assertFalse(arraysProcessing.checkArray(testArraysForCheck));
        Assertions.assertFalse(arraysProcessing.checkArray(testArraysForCheck));

    }

    private static Stream<Arguments> getTestArrays() {

        int[][] testArrayGet = {
                {8,6,3,7},
                {7,6,2,4,5,6,7,2},
                {1,2,4,2,4,5,3,4},
                {1,1,1,1,1,1},
                {1,2,3,4,33}
        };
        int[][] testArrayCheck = {
                {0,2,3,8},
                {4,4,1,4,4},
                {4,4,4,1,4,1,3,1},
                {1,1,1,1,1,1},
                {4,4,4,4,4}
        };

        return Stream.of(
                Arguments.of((Object) testArrayGet),
                Arguments.of((Object) testArrayCheck));
    }


}
