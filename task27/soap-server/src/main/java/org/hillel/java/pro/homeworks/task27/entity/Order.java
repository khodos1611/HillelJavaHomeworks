package org.hillel.java.pro.homeworks.task27.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@ToString

@XmlAccessorType(XmlAccessType.FIELD)
public class Order {

    private UUID id;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement
    private LocalDate date;
    private double cost;

    @XmlElement
    private List<Product> products;

    public Order() {
        this.id = UUID.randomUUID();
        this.cost = 0;
        this.date = LocalDate.now();
        this.products = new ArrayList<>();
    }
}
