package org.hillel.java.pro.homeworks.task27;

import lombok.SneakyThrows;
import org.hillel.java.pro.homeworks.task27.service.OrderRepository;

import javax.xml.ws.Endpoint;

public class Main {

    private static final String ADDRESS = "http://localhost:8585/orders?wsdl";

    private static final  Object SERVICE = new OrderRepository();

    public static void main(String[] args) {



        Endpoint endpoint = Endpoint.publish(ADDRESS, SERVICE);

        System.out.println("service is starting");
        sleep();

        endpoint.stop();


    }

    @SneakyThrows
    private static void sleep() {

        while (true)
            Thread.sleep(10000000);
    }
}