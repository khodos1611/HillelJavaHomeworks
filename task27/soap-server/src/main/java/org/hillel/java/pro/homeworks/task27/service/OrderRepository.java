package org.hillel.java.pro.homeworks.task27.service;

import org.hillel.java.pro.homeworks.task27.entity.Order;

import javax.jws.WebService;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebService
public class OrderRepository {

    Map<UUID, Order> orderMap = new HashMap<>();

    public void add(Order order){

        orderMap.put(order.getId(), order);
    }

    public Order getById(UUID id){
        return orderMap.get(id);
    }

    public Collection<Order> getAll(){

        return orderMap.values();

    }

}
