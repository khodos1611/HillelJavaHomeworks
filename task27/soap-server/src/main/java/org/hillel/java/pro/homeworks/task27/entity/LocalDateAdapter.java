package org.hillel.java.pro.homeworks.task27.entity;

import lombok.SneakyThrows;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;


public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    @SneakyThrows
    public LocalDate unmarshal(String v) {
        return LocalDate.parse(v);
    }
    @SneakyThrows
    public String marshal(LocalDate v) {
        return v.toString();
    }
}
