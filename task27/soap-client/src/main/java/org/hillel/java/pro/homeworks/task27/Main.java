package org.hillel.java.pro.homeworks.task27;

import lombok.SneakyThrows;
import org.hillel.java.pro.homeworks.task27.client.generated.Order;
import org.hillel.java.pro.homeworks.task27.client.generated.OrderRepository;
import org.hillel.java.pro.homeworks.task27.client.generated.OrderRepositoryService;
import org.hillel.java.pro.homeworks.task27.client.generated.Product;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");


        OrderRepository orderService  = new OrderRepositoryService().getOrderRepositoryPort();

        Product product1 = new Product();
        product1.setName("apple");
        product1.setCost(2);
        product1.setId(UUID.randomUUID().toString());

        Product product2 = new Product();
        product2.setName("lemon");
        product2.setCost(1);
        product2.setId(UUID.randomUUID().toString());

        List<Product> list = new ArrayList<>();
        list.add(product1);
        list.add(product2);


        Order order = new Order();
        order.setCost(5);
        order.setId(UUID.randomUUID().toString());
        order.setDate(LocalDate.now().toString());
        order.getProducts().addAll(list);

        orderService.add(order);
        orderService.getAll();
        orderService.getById("6b93e1b9-e2bb-4f9b-860c-025cad5a1158");

    }
}