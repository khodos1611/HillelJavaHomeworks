package org.hillel.filelogger;

import org.hillel.LoggingLevel;
import org.hillel.logger.Configuration;

public class FileLoggerConfiguration extends Configuration {

    public int maxSize;
    public String fileName;

    public FileLoggerConfiguration(String fileName, LoggingLevel loggingLevel, int maxSize, String format) {
        this.fileName = fileName;
        this.loggingLevel = loggingLevel;
        this.maxSize = maxSize;
        this.format = format;

    }

    @Override
    public String toString() {
        return "FileLoggerConfiguration{" +
                "maxSize=" + maxSize +
                ", fileName='" + fileName + '\'' +
                ", loggingLevel=" + loggingLevel +
                ", format='" + format + '\'' +
                '}';
    }
}
