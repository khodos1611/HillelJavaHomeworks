package org.hillel.filelogger;

public class FileMaxSizeReachedException extends RuntimeException{

    private long maxSize;
    private long currentSize;
    private String path;

    public FileMaxSizeReachedException(String message, long maxSize, long currentSize, String path) {
        super(message);
        this.maxSize = maxSize;
        this.currentSize = currentSize;
        this.path = path;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public long getCurrentSize() {
        return currentSize;
    }

    public String getPath() {
        return path;
    }
}
