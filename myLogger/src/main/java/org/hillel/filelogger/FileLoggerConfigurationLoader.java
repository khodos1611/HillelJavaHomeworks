package org.hillel.filelogger;

import org.hillel.LoggingLevel;
import org.hillel.logger.ConfigurationLoader;

import java.io.IOException;
import java.util.HashMap;

public class FileLoggerConfigurationLoader extends ConfigurationLoader {

    @Override
    public FileLoggerConfiguration load(String fileName) throws IOException {

        HashMap<String, String> configMap = new HashMap<>();
        configMap.put("FILE", "./");
        configMap.put("LEVEL", "DEBUG");
        configMap.put("MAX-SIZE", "250");
        configMap.put("FORMAT", ".txt");

        return configBuilder(super.getSettings(configMap, fileName));

    }

    private FileLoggerConfiguration configBuilder(HashMap<String, String> config){

        int size = getSize(config.get("MAX-SIZE"));

        return new FileLoggerConfiguration(config.get("FILE")
                    , LoggingLevel.valueOf(config.get("LEVEL"))
                    ,size,config.get("FORMAT"));
    }

    private int getSize(String size) {
        try {
            return Integer.parseInt(size);
        } catch (NumberFormatException ex){
            return 100;
        }
    }

}
