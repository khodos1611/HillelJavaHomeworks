package org.hillel.filelogger;

import org.hillel.LoggingLevel;
import org.hillel.logger.Logger;

import java.io.Writer;
import java.io.FileWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLogger implements Logger {

    FileLoggerConfiguration fileLoggerConfiguration;


    public FileLogger(String configFileName) throws IOException {

        FileLoggerConfigurationLoader loader = new FileLoggerConfigurationLoader();

        this.fileLoggerConfiguration = loader.load(configFileName);
    }

    @Override
    public void debug(String message) throws FileMaxSizeReachedException, IOException {

        if (fileLoggerConfiguration.loggingLevel != LoggingLevel.INFO) {
            String stringToFile = getStringToSend(message, LoggingLevel.DEBUG);

            sendMessage(stringToFile);
        }
    }

    @Override
    public void info(String message) throws FileMaxSizeReachedException, IOException {

        String stringToFile = getStringToSend(message, LoggingLevel.INFO);

        sendMessage(stringToFile);
    }


    public void sendMessage(String message) throws IOException {

        try {
            checkSizeObjectOfOutput();
        }
        catch (FileMaxSizeReachedException ex){
            System.out.println("Message: " + ex.getMessage());
            System.out.println("Max size :" + ex.getMaxSize()+
                    ", reached size : "+ex.getCurrentSize()+
                    ", file path : " + ex.getPath());

            File file = new File(fileLoggerConfiguration.fileName+"\\log"+fileLoggerConfiguration.format);
            String newFileName = fileLoggerConfiguration.fileName+"\\log_"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd_HH.mm.ss"))+fileLoggerConfiguration.format;
            try {
                File newFile = new File(newFileName);
                Files.move(file.toPath(), newFile.toPath());
            }catch(Exception e) {
                System.out.println(e.getMessage());
            }
            try (Writer writer = new FileWriter(fileLoggerConfiguration.fileName+"\\log"+fileLoggerConfiguration.format)) {
                writer.write("");
            }
        }

        finally {
            try (OutputStreamWriter writer =
                         new OutputStreamWriter(new FileOutputStream(fileLoggerConfiguration.fileName+"\\log"+fileLoggerConfiguration.format, true), "cp1251")) {
                writer.write(message.toCharArray());
            }
        }
    }


    public String getStringToSend(String message, LoggingLevel level){

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("["+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss"))+"] ");
        stringBuilder.append("["+level.toString()+"] ");
        stringBuilder.append("[message : " +message+"]").append(System.lineSeparator());

        return stringBuilder.toString();
    }


    public void checkSizeObjectOfOutput() throws FileMaxSizeReachedException {


        File file = new File(fileLoggerConfiguration.fileName+"\\log"+fileLoggerConfiguration.format);
        long fileLength = file.length();


        if (fileLength >= fileLoggerConfiguration.maxSize){
            throw new FileMaxSizeReachedException("Reached max file size "
                    ,fileLoggerConfiguration.maxSize,
                    fileLength,
                    file.getPath());
        }

    }
}
