package org.hillel.logger;

import org.hillel.LoggingLevel;

public abstract class Configuration {
    public LoggingLevel loggingLevel;

    public String format;

}
