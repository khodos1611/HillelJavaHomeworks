package org.hillel.logger;

import java.io.*;
import java.util.HashMap;
import java.util.Objects;
public abstract class ConfigurationLoader {

     public abstract Configuration load(String fileName) throws IOException;
    protected HashMap<String, String> getSettings(HashMap<String, String> defaultSettings, String configFileName) {

        HashMap<String, String> configMap = new HashMap<>();

        try (FileInputStream inputStream = new FileInputStream(this.getClass().getClassLoader().getResource(configFileName).getFile());
             Reader reader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(reader)
        ) {
            String readLine = "";
            StringBuilder value = new StringBuilder();
            do {
                readLine = bufferedReader.readLine();
                if (readLine == null || readLine.equals("")) continue;
                String[] words = readLine.split(":");

                for (int i = 1; i < words.length; i++) {
                    value.append(words[i].trim());
                }
                configMap.put(words[0].trim(), value.toString());
                value.delete(0, value.length());

            }while (Objects.nonNull(readLine));
        }
        catch (NullPointerException ex){
            System.out.println("No such file found.");
            return defaultSettings;
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
            return defaultSettings;
        }
        return configMap;

    }

}
