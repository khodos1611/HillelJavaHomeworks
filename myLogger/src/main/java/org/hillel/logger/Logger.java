package org.hillel.logger;

import java.io.IOException;

public interface Logger {

    void debug(String message) throws IOException;

    void info(String message) throws IOException;

}
