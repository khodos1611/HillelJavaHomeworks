package org.hillel.logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;

class TestConfiguration extends ConfigurationLoader{
    @Override
    public Configuration load(String fileName) throws IOException {
        return null;
    }
}
public class ConfigurationLoaderIT {

    @Test

    void loadTest() throws IOException {

        TestConfiguration testConfiguration = new TestConfiguration();

        HashMap<String, String> configMap = new HashMap<>();

        configMap = testConfiguration.getSettings(configMap, "config.info");

        Assertions.assertEquals(configMap.size(), 4);

    }

    @Test

    void loadNoSuchFileTest() throws IOException {

        TestConfiguration testConfiguration = new TestConfiguration();

        HashMap<String, String> configMap = new HashMap<>();

        configMap = testConfiguration.getSettings(configMap, "config12.info");

        Assertions.assertEquals(configMap.size(), 0);

    }

}
