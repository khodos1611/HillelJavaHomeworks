package org.hillel.filelogger;

import java.io.IOException;

import org.hillel.LoggingLevel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FileLoggerConfigurationLoaderIT {

    @Test

    void  loadTest() throws IOException {
        FileLoggerConfigurationLoader fileLoggerConfigurationLoader = new FileLoggerConfigurationLoader();

        FileLoggerConfiguration testConfiguration = new FileLoggerConfiguration("./", LoggingLevel.DEBUG, 150, ".txt");

        Assertions.assertEquals(testConfiguration.toString(), fileLoggerConfigurationLoader.load("configEnoughSize.info").toString());
    }

    @Test
    void getSizeTest() throws IOException {

        FileLoggerConfigurationLoader fileLoggerConfigurationLoader = new FileLoggerConfigurationLoader();

        Assertions.assertEquals(100, fileLoggerConfigurationLoader.load("configBadSize.info").maxSize);


    }
}
