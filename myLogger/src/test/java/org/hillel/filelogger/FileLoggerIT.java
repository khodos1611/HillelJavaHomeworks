package org.hillel.filelogger;

import org.hillel.LoggingLevel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLoggerIT {

    @Test
    void FileLoggerTest()  {
        Assertions.assertDoesNotThrow(() -> new FileLogger("configEnoughSize.info"));
    }

    @Test
    void debugDebugTest() throws IOException {

        FileLogger fileLogger= new FileLogger("config.info");

        Assertions.assertDoesNotThrow(() -> fileLogger.debug("debug"));
    }

    @Test
    void debugInfoTest() throws IOException {

        FileLogger fileLogger= new FileLogger("configSmall.info");

        Assertions.assertDoesNotThrow(() -> fileLogger.debug("debug"));
    }
    @Test
    void  infoTest(){
        Assertions.assertDoesNotThrow(() -> new FileLogger("config.info").info("info"));
    }

    @Test
    void sendMessageExceptionTest(){

        FileMaxSizeReachedException exception =
                Assertions.assertThrows(FileMaxSizeReachedException.class, () -> new FileLogger("configSmall.info").checkSizeObjectOfOutput());

        Assertions.assertEquals("Reached max file size ", exception.getMessage());

    }
    @Test
    void sendMessageTest(){

        Assertions.assertDoesNotThrow(() -> new FileLogger("configEnoughSize.info").sendMessage("message"));
    }

    @Test
    void getStringToSentDebugTest() throws IOException {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("["+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss"))+"] ");
        stringBuilder.append("["+ LoggingLevel.DEBUG+"] ");
        stringBuilder.append("[message : DEBUG]").append(System.lineSeparator());

        System.out.println("stringBuilder = " + stringBuilder);
        Assertions.assertEquals(stringBuilder.toString(), new FileLogger("config.info").getStringToSend("DEBUG",LoggingLevel.DEBUG));


    }
    @Test
    void getStringToSentInfoTest() throws IOException {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("["+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss"))+"] ");
        stringBuilder.append("["+ LoggingLevel.INFO+"] ");
        stringBuilder.append("[message : INFO]").append(System.lineSeparator());

        System.out.println("stringBuilder = " + stringBuilder);
        Assertions.assertEquals(stringBuilder.toString(), new FileLogger("config.info").getStringToSend("INFO",LoggingLevel.INFO));


    }
    @Test
    void checkSizeObjectOfOutputTest(){

       FileMaxSizeReachedException exception =
            Assertions.assertThrows(FileMaxSizeReachedException.class, () -> new FileLogger("configSmall.info").checkSizeObjectOfOutput());
       Assertions.assertEquals("Reached max file size ", exception.getMessage());

    }


}
