package org.hillel.java.pro.homeworks.petrolstation;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Semaphore;
import java.util.stream.Stream;

@Slf4j
public class Main {

    @SneakyThrows
    public static void main(String[] args) {

        Semaphore station = new Semaphore(3, true);
        PetrolStation petrolStation = new PetrolStation(15);

        Stream.generate(() -> new Client(petrolStation, station))
                .limit(5)
                .forEach(Thread::start);

        Thread.sleep(10000);

        log.info("{}",petrolStation.amount);
    }
}

