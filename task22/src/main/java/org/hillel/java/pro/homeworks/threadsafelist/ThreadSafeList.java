package org.hillel.java.pro.homeworks.threadsafelist;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ThreadSafeList {

    List<String > list = new ArrayList<>();

    ReadWriteLock lock = new ReentrantReadWriteLock();
    Lock writeLock = lock.writeLock();
    Lock readLock = lock.readLock();

    public void remove(int id){
        try {
            writeLock.lock();
            System.out.println("Remove item with id " + id);
            list.remove(id);

        }
        catch (IndexOutOfBoundsException ex){
            System.out.println(ex.getMessage());
        }
        finally {
            writeLock.unlock();
        }
    }

    public void add(String object){
        try {
            writeLock.lock();
            System.out.println("Add item " + object);
            list.add(object);
        } finally {
            writeLock.unlock();
        }
    }

    public String get(int id){
        try {
            readLock.lock();
            System.out.println("Get item with id " + id);
            return list.get(id);
        }
        catch (IndexOutOfBoundsException ex){
            return null;
        }
        finally {
            readLock.unlock();
        }
    }
}
