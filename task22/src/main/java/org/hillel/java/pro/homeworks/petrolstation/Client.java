package org.hillel.java.pro.homeworks.petrolstation;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;


@Slf4j
public class Client extends Thread{
    PetrolStation petrolStation;
    Semaphore station;
    Client(PetrolStation petrolStation, Semaphore station){
        this.petrolStation = petrolStation;
        this.station = station;

        log.info("{} is run", this.getName());
    }
    @Override
    @SneakyThrows
    public void run() {

            if (petrolStation.verifyAmount(this)) {
                log.info("{} waiting in queue", this.getName());
                station.acquire();
                log.info("{} try to do refuel", this.getName());
                sleep(ThreadLocalRandom.current().nextInt(3, 10) * 1000);

                petrolStation.doRefuel();
                log.info("{} finished", this.getName());
                station.release();
            } else log.error("{} ran out of gas", this.getName());



    }
}
