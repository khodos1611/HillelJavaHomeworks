package org.hillel.java.pro.homeworks.petrolstation;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class PetrolStation {

    double amount;

    int reserved=0;
    final double LIMIT = 5;

    ReentrantLock lock = new ReentrantLock();

    public PetrolStation(double amount) {
        this.amount = amount;
    }

    @SneakyThrows
    public double doRefuel() {

        lock.lock();
        reserved--;
        amount -= LIMIT;

        log.info("left {}", amount);

        Thread.sleep(500);

        lock.unlock();
        return amount;
    }

    @SneakyThrows
    public boolean verifyAmount(Client client) {

        boolean result;

        lock.lock();
        reserved++;
        log.info("reserved ++ : {}", reserved);
        log.info("{} in verify", client.getName());

        if ((amount-reserved*LIMIT) >= 0) {
            log.info("{} enough", client.getName());

            result = true;
        } else {
            reserved--;
            log.info("not enough, reserved -- : {}", reserved);
            result = false;}

        lock.unlock();
        return result;

    }

}

