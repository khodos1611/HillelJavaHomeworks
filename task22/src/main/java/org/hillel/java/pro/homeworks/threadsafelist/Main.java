package org.hillel.java.pro.homeworks.threadsafelist;

import com.github.javafaker.Faker;

import java.util.Random;

public class Main {

    static Faker faker = new Faker();

    static  ThreadSafeList threadSafeList = new ThreadSafeList();

    public static void main(String[] args) throws InterruptedException {

        new Writer().start();
        new Writer().start();
        new Writer().start();
        new Writer().start();
        new Writer().start();
        Thread.sleep(1000);

        new Reader().start();
        new Reader().start();
        new Reader().start();
        new Reader().start();
        new Reader().start();
        Thread.sleep(1000);

        new Remover().start();
        new Remover().start();
        new Remover().start();
        new Remover().start();

    }

    static class Writer extends Thread {

        @Override
        public void run() {
            threadSafeList.add(faker.animal().name());
        }
    }

    static class Reader extends Thread {

        @Override
        public void run() {
            int id = new Random().nextInt(11);
            threadSafeList.get(id);
        }
    }

    static class Remover extends Thread {

        @Override
        public void run() {
            int id = new Random().nextInt(11);
            threadSafeList.remove(id);
        }
    }
}