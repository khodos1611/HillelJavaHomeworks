package org.hillel.java.pro.homeworks;

import lombok.SneakyThrows;

public class ValueCalculator {

    final int size=1_000_000;
    final int halfSize=500_000;
    float[] array = new float[size];

    @SneakyThrows
    public void method() {

        long startExecution = System.currentTimeMillis();

        for (int i = 0; i < size; i++) {
           array[i] = 1;
        }

        float[] a1 = new float[halfSize];
        float[] a2 = new float[halfSize];

        System.arraycopy( array, 0 , a1, 0, halfSize);
        System.arraycopy( array, halfSize, a2, 0, halfSize);

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < halfSize; i++) {
               a1[i] =  (float)(a1[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < halfSize; i++) {
                a2[i] =  (float)(a2[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
            }

        });

        t1.start();
        t2.start();
        t1.join();
        t2.join();


        System.arraycopy(a1, 0, array, 0, halfSize);
        System.arraycopy(a2, 0, array, halfSize, halfSize);

        long timeExecution = System.currentTimeMillis() - startExecution;
        System.out.println("Execution time = "+timeExecution + " milliseconds");
    }
}
