package org.hillel.java.pro.homeworks;

import org.hillel.java.pro.homeworks.TestClasses.TestClass1;
import org.hillel.java.pro.homeworks.TestClasses.TestClass2;
import org.hillel.java.pro.homeworks.TestClasses.TestClass3;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRunnerTest {

    @Test
    void startTest(){
        Assertions.assertDoesNotThrow(()->TestRunner.start(TestClass1.class));

        BeforeAfterSuitException exception1 =
                Assertions.assertThrows(BeforeAfterSuitException.class, ()->TestRunner.start(TestClass2.class));

        Assertions.assertEquals("Must be only one BeforeSuit method in class", exception1.getMessage());

        BeforeAfterSuitException exception2 =
                Assertions.assertThrows(BeforeAfterSuitException.class, ()->TestRunner.start(TestClass3.class));

        Assertions.assertEquals("Must be only one AfterSuit method in class", exception2.getMessage());

    }
}
