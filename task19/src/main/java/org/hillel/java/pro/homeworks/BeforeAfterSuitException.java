package org.hillel.java.pro.homeworks;

import lombok.Getter;

@Getter
public class BeforeAfterSuitException extends RuntimeException{

    public BeforeAfterSuitException(String message) {
        super(message);
    }
}


