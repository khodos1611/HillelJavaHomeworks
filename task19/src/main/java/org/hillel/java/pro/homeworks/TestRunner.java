package org.hillel.java.pro.homeworks;

import lombok.SneakyThrows;
import org.hillel.java.pro.homeworks.Annotations.AfterSuit;
import org.hillel.java.pro.homeworks.Annotations.BeforeSuit;
import org.hillel.java.pro.homeworks.Annotations.TestHillel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;

public class TestRunner {

    @SneakyThrows
    public static void start(Class cls) throws BeforeAfterSuitException{

        checkBeforeAfterSuit(cls, BeforeSuit.class, "Must be only one BeforeSuit method in class");

        System.out.println(cls.getSimpleName());

        Arrays.stream(cls.getDeclaredMethods()).filter(c->c.isAnnotationPresent(TestHillel.class))
                .sorted(Comparator.comparing(c->c.getDeclaredAnnotation(TestHillel.class).priority()))
                .forEach(c-> callMethod(c));

        checkBeforeAfterSuit(cls, AfterSuit.class, "Must be only one AfterSuit method in class");

    }

    public static void callMethod(Method method){
        try {
            method.invoke(null);
            System.out.println(" priority = "+method.getDeclaredAnnotation(TestHillel.class).priority());
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

    }
    @SneakyThrows
    public static void checkBeforeAfterSuit(Class clsSource, Class clsCheck, String message){
       Arrays.stream(clsSource.getDeclaredMethods()).filter(c->c.isAnnotationPresent(clsCheck))
               .reduce((x,y)->{
                   if(y==null) callMethod(x);
                   else throw new BeforeAfterSuitException(message);
                   return null;
               });
    }
}
