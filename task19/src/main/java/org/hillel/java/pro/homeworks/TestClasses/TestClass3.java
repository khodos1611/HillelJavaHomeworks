package org.hillel.java.pro.homeworks.TestClasses;

import org.hillel.java.pro.homeworks.Annotations.AfterSuit;
import org.hillel.java.pro.homeworks.Annotations.BeforeSuit;
import org.hillel.java.pro.homeworks.Annotations.TestHillel;

public class TestClass3 {
    @TestHillel(priority = 1)
    public static void method1() {
        System.out.println("method1 is run");
    }
    public static void method2() {
        System.out.println("method2 is run");
    }
    @TestHillel(priority = 2)
    public static void method3() {
        System.out.println("method3 is run");
    }
    @AfterSuit
    public static void method4() {
        System.out.println("After suit method is run");
    }
    @AfterSuit
    public static void method6() {
        System.out.println("After suit method is run");
    }
    @BeforeSuit
    public static void method8() {
        System.out.println("Before suit method is run");
    }
    @TestHillel(priority = 3)
    public static void method7() {
        System.out.println("method7 is run");
    }

}
