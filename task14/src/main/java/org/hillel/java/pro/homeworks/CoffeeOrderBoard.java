package org.hillel.java.pro.homeworks;

import java.util.LinkedList;
import java.util.Queue;

public class CoffeeOrderBoard {

    Queue<Order> orderQueue = new LinkedList<Order>();
    int orderNumber=1;

   public void add(String name){

       orderQueue.add(Order.builder()
                .name(name)
                .number(orderNumber++)
                .build());
    }

    public void deliver(){
        orderQueue.poll();
    }

    public void deliver(int number){

       orderQueue.removeIf(order -> order.getNumber()==number);

    }

    public void draw(){
        orderQueue.forEach(System.out::println);
    }

}
