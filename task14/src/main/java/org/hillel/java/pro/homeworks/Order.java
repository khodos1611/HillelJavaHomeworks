package org.hillel.java.pro.homeworks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class Order {
    private int number;
    private String name;

}
