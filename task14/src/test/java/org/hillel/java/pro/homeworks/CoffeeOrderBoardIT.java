package org.hillel.java.pro.homeworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class CoffeeOrderBoardIT {

    @Test
    void CoffeeOrderBoardTest(){

        CoffeeOrderBoard orderBoard = new CoffeeOrderBoard();

        orderBoard.add("Guest #1");
        orderBoard.add("Guest #2");
        orderBoard.add("Guest #3");
        System.out.println(" ========3");
        orderBoard.draw();

        orderBoard.add("Guest #4");
        orderBoard.add("Guest #5");
        orderBoard.add("Guest #6");
        System.out.println(" ========6");
        orderBoard.draw();

        System.out.println(" ==== remove 5");
        orderBoard.deliver(5);
        orderBoard.draw();

        System.out.println(" ==== remove 6");
        orderBoard.deliver(6);
        orderBoard.draw();

        System.out.println(" ==== remove 7");
        orderBoard.deliver(7);
        orderBoard.draw();

        System.out.println(" ====== ");
        orderBoard.deliver();
        orderBoard.draw();
        System.out.println(" ====== ");
        orderBoard.deliver();
        orderBoard.draw();
        System.out.println(" ====== ");
        orderBoard.deliver();
        orderBoard.draw();
        orderBoard.add("Guest #7");
        orderBoard.add("Guest #8");
        orderBoard.add("Guest #9");
        orderBoard.add("Guest #10");
        System.out.println(" ====== ");
        orderBoard.draw();


        Assertions.assertEquals(5, orderBoard.orderQueue.size());
        Assertions.assertEquals(4, orderBoard.orderQueue.element().getNumber());
    }
}
