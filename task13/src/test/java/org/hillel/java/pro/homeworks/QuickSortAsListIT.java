package org.hillel.java.pro.homeworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Arrays;

public class QuickSortAsListIT {


    @Test
    void testSort(){

        Integer[] testArray2 = {7,6,2,4,5,61,71,29};

        Assertions.assertEquals(2, QuickSortAsList.sort(Arrays.asList(testArray2)).get(0));
    }


}
