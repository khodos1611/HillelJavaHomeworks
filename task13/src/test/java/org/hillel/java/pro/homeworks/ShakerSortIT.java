package org.hillel.java.pro.homeworks;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ShakerSortIT {

    @Test
    void shakerSortTest(){

        ShakerSort shakerSort = new ShakerSort();

        int[] testArray = {7,6,2,4,5,61,71,29};

        Assertions.assertTrue(ArrayUtils.isSorted(shakerSort.sort(testArray)));
    }
}
