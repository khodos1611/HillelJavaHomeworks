package org.hillel.java.pro.homeworks;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class QuickSortIT {

    @Test
    void quickSortTest(){

        int[] testArray = {7,6,2,4,5,61,71,29};
        QuickSort quickSort = new QuickSort();
        quickSort.sort(testArray, 0, testArray.length-1);
        System.out.println("Arrays.toString(testArray) = " + Arrays.toString(testArray));
        Assertions.assertTrue(ArrayUtils.isSorted(testArray));
    }
}
