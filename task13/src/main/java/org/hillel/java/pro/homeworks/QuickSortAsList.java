package org.hillel.java.pro.homeworks;

import org.hillel.java.pro.homeworks.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class QuickSortAsList {

     public static List<Integer> sort(List<Integer> source){

         List<Integer> left = new ArrayList<>();
         List<Integer> right = new ArrayList<>();


         if (source.size()<=1) return source;
         int controlValue = source.get(source.size()-1);

         for (Integer element: source) {
             if (element==controlValue) continue;
             if ( element < controlValue) left.add(element);
             else right.add(element);
         }

        return Utils.mergeList(sort(left), sort(right), controlValue);
    }


}
