package org.hillel.java.pro.homeworks.utils;

import java.util.List;

public class Utils {

    public static  void changePlaces(int[] source, int i, int j) {
        int temp = source[i];
        source[i] = source[j];
        source[j] = temp;
    }

    public static List<Integer> mergeList(List<Integer> a, List<Integer> b, Integer value){

        a.add(value);
        a.addAll(b);

        return a;
    }
}
