package org.hillel.java.pro.homeworks;

import org.hillel.java.pro.homeworks.utils.Utils;

public class ShakerSort {

    public int[] sort(int[] source) {
        int left = 0;
        int right = source.length - 1;
        while (left < right) {
            for (int i = left; i < right; i++) {
                if (source[i] > source[i + 1]) {
                    Utils.changePlaces(source, i, i + 1);
                }
            }
            right--;
            for (int i = right; i > left; i--) {
                if (source[i] < source[i - 1]) {
                    Utils.changePlaces(source, i, i - 1);
                }
            }
            left++;
        }
        return source;
    }
}
