package org.hillel.java.pro.homeworks;

import org.hillel.java.pro.homeworks.utils.Utils;

public class QuickSort {

    public void sort(int[] source, int start, int end) {

        int i = start;
        int j = end;

        int p = source[end];

        do {
            while ( source[i] < p ) i++;
            while ( source[j] > p ) j--;

            if (i <= j) {
                Utils.changePlaces(source, i, j);
                i++; j--;
            }
        } while ( i<=j );

        if ( start < j ) sort(source, start, j);
        if ( i < end ) sort(source,i, end);
    }


}
