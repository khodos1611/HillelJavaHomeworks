package org.hillel.java.pro.homeworks;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Node {
    private int value;
    private Node leftNode;
    private Node rightNode;

}
