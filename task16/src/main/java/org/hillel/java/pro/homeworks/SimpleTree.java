package org.hillel.java.pro.homeworks;

import lombok.Getter;
import lombok.ToString;

import java.util.LinkedList;
import java.util.Queue;

@Getter
@ToString
public class SimpleTree {

    private Node root;

    public SimpleTree(){
        this.root = null;
    }

    public void addNode(int value){
        Node newNode = new Node();
        newNode.setValue(value);
        if (root == null) {
            root = newNode;
        }
        else {
            Node current = root;
            Node parent;
            while (true)
            {
                parent = current;
                if(value == current.getValue()) {
                    return;
                }
                else  if (value < current.getValue()) {
                    current = current.getLeftNode();
                    if (current == null){
                        parent.setLeftNode(newNode);
                        return;
                    }
                }
                else {
                    current = current.getRightNode();
                    if (current == null) {
                        parent.setRightNode(newNode);
                        return;
                    }
                }
            }
        }
    }
    public void drawTreeDepth(Node current) {
        if (current == null) {
            System.out.println("leaf");
            return;
        }
        int currentValue = current.getValue();
        System.out.println(currentValue);
        drawTreeDepth(current.getLeftNode());
        drawTreeDepth(current.getRightNode());

    }

    public void drawTreeWidth(){
        Queue<Node> nodeQueue = new LinkedList<>();

        Node current = root;

        nodeQueue.add(current);
        while (!nodeQueue.isEmpty()){
            Node tempNode = nodeQueue.poll();
            System.out.println(tempNode.getValue());
            Node leftNode = tempNode.getLeftNode();
            Node rightNode = tempNode.getRightNode();
            if (leftNode != null) nodeQueue.add(leftNode); else System.out.println("leaf");
            if (rightNode != null) nodeQueue.add(rightNode); else System.out.println("leaf");
        }
    }
}
