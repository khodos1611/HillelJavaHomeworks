package org.hillel.java.pro.homeworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpleTreeTest {

    @Test
    void testSimpleTree(){

        SimpleTree tree = new SimpleTree();
        tree.addNode(8);
        tree.addNode(3);
        tree.addNode(6);
        tree.addNode(14);
        tree.addNode(10);
        tree.addNode(1);
        tree.addNode(4);
        tree.addNode(7);
        tree.addNode(13);

        tree.drawTreeDepth(tree.getRoot());
        System.out.println(" ------------ ");
        tree.drawTreeWidth();
        Assertions.assertEquals(8, tree.getRoot().getValue());



    }
}
