package org.hillel.java.pro.homeworks.phonebook;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {

    public List<Record> recordList = new ArrayList<>();

    public void add(String name, String phone){
        recordList.add(Record.builder().name(name).phone(phone).build());
    }

    public Record find(String name){

        for (Record rec: recordList) {
            if (rec.getName().equals(name)) return rec;
        }

        return null;
    }

    public List<Record> findAll(String name){

        List<Record> resultList = new ArrayList<>();

        for (Record rec: recordList) {
            if (rec.getName().equals(name)) resultList.add(rec);
        }
        return (resultList.size()==0)?null:resultList;
    }
}
