package org.hillel.java.pro.homeworks.phonebook;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class Record {
    private String name;
    private String phone;

}
