package org.hillel.java.pro.homeworks.listProcessing;


import org.hillel.java.pro.homeworks.dto.Occurrence;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;


public class ListProcessing {

    public static int countOccurrence(List<String> words, String searchWord){

        int count=0;
        for (String word:words) {
            if (word.equals(searchWord)) count++;
        }
        return count;
    }

    public static List<String> toList(String[] source){
        return Arrays.asList(source);
    }

    public static List<?> findUnique(List<?> source){

        List<Object> resultList = new ArrayList<>();
        for (Object num: source) {
            if (! resultList.contains(num)) resultList.add(num);
        }
        return resultList;
    }
    public static void calcOccurrence(List<String> source){

        List<?> keys = findUnique(source);

        for (Object key: keys) {
            System.out.println(key + ":" + countOccurrence(source,key.toString()));
        }

    }

    public static List<Occurrence> findOccurrence(List<String> source){

        List<Occurrence> resultList = new ArrayList<>();

        List<?> keys = findUnique(source);

        for (Object key: keys) {
            Occurrence occurrence = Occurrence.builder()
                    .name(key.toString())
                    .count(countOccurrence(source,key.toString())).build();
            resultList.add(occurrence);
        }

        return resultList;
    }
}
