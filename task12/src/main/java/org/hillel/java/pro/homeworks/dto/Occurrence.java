package org.hillel.java.pro.homeworks.dto;

import lombok.Builder;
import lombok.Setter;

@Builder
@Setter
public class Occurrence {
    private final  String name;
    private final int count;

}
