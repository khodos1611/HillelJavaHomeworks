package org.hillel.java.pro.homeworks.listProcessing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

public class ListProcessingIT {

    public List<String> words = new ArrayList<>();

    @BeforeEach
    public void init(){
        words.add("pear");
        words.add("pear");
        words.add("nut");
        words.add("melon");
        words.add("pumpkin");
        words.add("apple");
        words.add("nut");
        words.add("nut");
        words.add("apple");
        words.add("pumpkin");
        words.add("pear");
        words.add("pumpkin");
        words.add("pumpkin");
        words.add("pear");
    }
    @ParameterizedTest()
    @ValueSource(strings = { "pear", "nut", "melon", "pumpkin" })
    void countOccurrenceTest(String searchWord){

        System.out.println("words.toString() = " + words.toString());
        System.out.println(" = " + ListProcessing.countOccurrence(words, searchWord));
        Assertions.assertNotEquals(0,ListProcessing.countOccurrence(words, searchWord));

    }

    @Test
    void toListTest(){

        String[]  array = {"cat", "dog", "snake"};

        Assertions.assertInstanceOf(List.class, ListProcessing.toList(array));

    }

    @Test
    void findUniqueTest(){
        List<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(2);
        nums.add(3);
        nums.add(4);
        nums.add(1);
        nums.add(5);
        nums.add(2);
        nums.add(1);
        nums.add(7);
        nums.add(15);

        System.out.println("nums.toString() = " + nums);
        System.out.println(ListProcessing.findUnique(nums));
        Assertions.assertEquals(7, ListProcessing.findUnique(nums).size());

    }

    @Test
    void findOccurrence(){

        Assertions.assertEquals(5, ListProcessing.findOccurrence(words).size());

    }

    @Test
    void calcOccurrence(){

        ListProcessing.calcOccurrence(words);

    }
}
