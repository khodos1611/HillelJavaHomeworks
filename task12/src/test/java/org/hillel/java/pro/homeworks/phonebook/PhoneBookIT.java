package org.hillel.java.pro.homeworks.phonebook;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class PhoneBookIT {

    PhoneBook phoneBook = new PhoneBook();

    @BeforeEach
    void init(){
        phoneBook.add("Petr", "111111");
        phoneBook.add("Ivan", "222222");
        phoneBook.add("David", "33333");
        phoneBook.add("Michael", "444444");

    }
    @Test
    
    void addTest(){
        
        phoneBook.add("Ivan", "34223423");

        System.out.println("phoneBook.recordList.get(0).toString() = " + phoneBook.recordList.toString());
    }

    @ParameterizedTest()
    @ValueSource(strings = { "Petr", "David", "Ivan", "Michael", "Oleg" })
    void findTest(String name){

        try {
            System.out.println(phoneBook.find(name).toString());
        }
        catch (NullPointerException ex){
            System.out.println("Not found any record with name "+name);
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = { "Ivan" , "Cris"})
    void findAllTest(String name){

        phoneBook.add("Ivan", "34223423");
        try {
            System.out.println(phoneBook.findAll(name).toString());
        }
        catch (NullPointerException ex){
            System.out.println("Not found records with name "+name);
        }
    }
}
